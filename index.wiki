= remote server = 

[[remote_server|remote_server]]


= nvim =

[[nvim]] _vim shortcuts_

= R = 

[[r-base]] _r-base notes_
[[r-cran]] _r notes on external packages_

= pandoc = 

[[pandoc.wiki|pandoc]] pandoc notes

= bash tools =
[[bash_tools.wiki|bash tools]]

= complex actions = 
[[complex_actions.wiki|complex actions]]

