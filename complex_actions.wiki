= Index =



= Remove files on conditions = 
== Remove files with a number of lines inferior to N==
{{{sh
find . -type f -exec sed 13q1 '{}' ';' -exec rm -f '{}' ';'
}}}
Here N=13, every files with a number of line < at 13 is removed
