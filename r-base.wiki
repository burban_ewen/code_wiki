= sheebang =
This line is mandatory if you want to make your script executable
{{{r
#!/usr/bin/env Rscript
}}}

= command arg =

{{{r
for (arg in commandArgs()){
    tmp = unlist(strsplit(arg,'=')); opt = tmp[1];var=tmp[2]
    if(opt=='dir'){dir=var}
}
}}}
In case where the behavior of all arg is <arg>=<arg value>, the next exemple
is better, because it allow to call arg values more easely. 
{{{r
# transform the arg vector into a name vector of arg value, name by the arg name
args = commandArgs(trailingOnly=T)
args = sapply(args,function(x){tmp = unlist(strsplit(x,split='='))
            y = vector()
            y[tmp[1]] = tmp[2]
            return(y)},USE.NAMES=F)
output = args['output']
}}}
= color =

== alpha color ==

{{{r
# transform a vector of col into the alphized version
add.alpha=function(col,alpha=1){
    apply(sapply(col,col2rgb)/255,2,function(x){rgb(x[1],x[2],x[3],alpha=alpha)})
}
}}}

= distribution query =

== minmax auto ==

{{{r
minmax = function(x){summary(x)[c('Min.','Max.')]}
}}}
